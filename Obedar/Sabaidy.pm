package Obedar::Sabaidy;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use strict;
use utf8;
use LWP::Simple;
use POSIX;
use HTML::Entities;

# my URL
sub my_url {
    return 'http://www.amphone.eu/poledni-menu';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;
    my $html = get(my_url());
    return (1, 'parsing has not saved the day today (no HTML)') unless $html;

    $html =~ s|^.*uk-container-center||s;
    $html =~ s|<section class="section-footer">.*$||s;
    $html =~ s{</strong>}{:}g;
    $html =~ s{</?(?:p|strong|em|li|ol|div)[^>]*>}{}g;
    $html = decode_entities($html);

    my @days = split /Pondělí|Úterý|Středa|Čtvrtek|Pátek/, $html;
    shift @days;

    return (1, 'parsing has not saved the day today (bad list)') unless @days == 5;

    my $den = +(localtime( time + $day*86400 ))[6];
    my $menu = $days[$den - 1];

    $menu =~ s/^[\s:]*//mg; # leading medzery a : (a : vzniknuta z $den</strong>)
    $menu =~ s/[\s:]*$//mg; # trailing medzery a :
    $menu =~ s/\n\n/\n/g; 
    $menu =~ s/ :/: /g; # korekcia zapisu dvojbodky

    return (1, 'parsing has not saved the day today (bad menu)') unless $menu;

    # success!
    return (0, $menu);
}

1;
