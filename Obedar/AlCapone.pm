package Obedar::AlCapone;

# vim: tabstop=4 softtabstop=4 expandtab shiftwidth=4

use open ':std', ':utf8';

use strict;
use utf8;
use LWP::Simple;
use POSIX;

# vlastni jmeno/oznaceni
sub my_name {
    return 'alcapone';
}

# URL menicka
sub my_url {
    return 'http://www.pizzaalcapone.cz/poledni-menu';
}

# get_menu
sub get_menu {
    my ($day) = @_;
    my $menu;

    # pozadovany den
    my $now_re = POSIX::strftime('%d.\s*%m.\s*%Y', localtime( time + $day*86400 ));

    # get menu
    my $html = get( my_url );

    # ceny
    my (%ceny) = $html =~ m#(Menu 1) - (\d+\s*Kč) / (Menu 2) - (\d+\s*Kč) / (Menu 3) - (\d+\s*Kč)#;

    $html =~ s/\r?\n\s*//gs;
    my (@data) = grep { /strong/ } $html =~ /<p class="poledni-menu[^>]*>(.*?)<\/p>/g;

    # vytahnout dnesni/pozadovany den
    my ($dnes) = join("\n", @data) =~ /^(.*$now_re.*)$/m;

    my (@d) = grep { $_ } map { $_ =~ s/<\/?[bp]>//g; $_; } split(/<br\s*\/?>/, $dnes);
    shift(@d); # zahodit uvodni den

    # doplnit ceny
    $menu = join("\n", map { my ($m) = $_ =~ /(Menu\s*\d+)/; if ( $m ) { $_ .= " [$ceny{$m}]"; } $_; } @d);
    $menu =~ s/\s*$//s;

    return (0, $menu);
}

1;
