package Config::Restaurants;

use utf8;

# https://www.chaps.cz/files/idos/IDOS-API.pdf
my $bus32 = 'Bus spoj: https://jizdnirady.idnes.cz/brno/spojeni/?f=Botanick%C3%A1&t=Su%C5%A1ilova&submit=true';

# primarne hladat Zomato, kedze odtial vieme data tahat cistym sposobom
# aj pri pouziti Zomato je ale fajn mat uvedene fallback_url, ak nevyplnia alebo co


our $restaurants = {
    'padthai' => {
        'type' => 'menicka',
        'menu_url' => 'https://www.menicka.cz/8483-padthai.html',
    },
    'everest' => {
        'type' => 'showurl',
        'aliases' => [qw(eve)],
        'menu_url' => 'https://www.everestbrno.cz/denni-menu/', # parseable
        # https://www.menicka.cz/8033-everest-nepalese-restaurant.html may not be filled out
        'note' => $bus32,
    },
    'selepka' => {
        'type' => 'showurl',
        'menu_url' => 'http://www.selepova.cz/denni-menu/', # parseable
        # https://www.menicka.cz/2721-selepka.html
    },
    'suzies' => {
        'type' => 'menicka',
        'menu_url' => 'https://www.menicka.cz/3830-suzies-steak-pub.html',
        'note' => $bus32,
    },
    'cattani' => {
        'type' => 'showurl',
        'aliases' => [qw(catani katani)],
        'menu_url' => 'http://www.cattani.cz/, https://www.menicka.cz/3179-cattani---josefska.html',
    },
    'udrevaka' => {
        'type' => 'menicka',
        'aliases' => [qw(drevak)],
        'menu_url' => 'https://www.menicka.cz/2752-u-drevaka-beergrill.html, https://udrevaka.cz/denni-menu', # parseable
    },
    'vegalite' => {
        'type' => 'showurl',
        'menu_url' => 'http://vegalite.cz/#denni-menu, https://www.menicka.cz/2775-vegalite.html', # parseable
    },
    'siwa' => {
        'type' => 'menicka',
        'menu_url' => 'https://www.menicka.cz/5340-siwa-orient.html',
    },
    'divabara' => {
        'type' => 'menicka',
        'menu_url' => 'https://www.menicka.cz/6468-diva-bara.html',
    },
    'annapurna' => {
        'type' => 'showurl',
        'menu_url' => 'http://indicka-restaurace-annapurna.cz/images/tydenijidelnicek/menu.pdf', # pdftotext + parseable
        'aliases' => [qw(anapurna)],
    },
    'tivoli' => {
        'type' => 'showurl',
        'menu_url' => 'http://tivolicafe.cz/menu/poledni-menu/',
    },
    'alcapone' => {
        'type' => 'module',
        'module' => 'Obedar::AlCapone',
        'fallback_url' => 'https://www.pizzaalcapone.cz/poledni-menu',
    },
    'lightofindia' => {
        'type' => 'module',
        'module' => 'Obedar::LightOfIndia',
        'aliases' => [qw(light)],
        'fallback_url' => 'http://www.lightofindia.cz/lang-cs/denni-menu',
    },
    'sabaidy' => {
        'type' => 'module',
        'module' => 'Obedar::Sabaidy',
        'fallback_url' => 'http://www.amphone.eu/poledni-menu',
    },
    'lastrada' => {
        'type' => 'module',
        'module' => 'Obedar::LaStrada',
        'fallback_url' => 'http://lastrada-brno.cz/poledni-menu/',
        'note' => $bus32,
    },
    'nepal' => {
        'type' => 'module',
        'module' => 'Obedar::NepalBrno',
    },
    'klubcestovatelu' => {
        'type' => 'module',
        'module' => 'Obedar::KlubCestovateluBrno',
    },
    'buddha' => {
        'type' => 'module',
        'module' => 'Obedar::Buddha',
        'note' => $bus32,
    },
    'khaybar' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/pg/Indická-Restaurace-Khaybar-184987895598026/posts/',
    },
    'veselacajovna' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.veselacajovna.cz/tydenni-nabidka/',
    },
    'haribol' => {
        'type' => 'nomenu',
    },
    'chutnestesti' => {
        'type' => 'nomenu',
    },
    'lokofu' => {
        'type' => 'nomenu',
    },
    'vietnam' => {
        'type' => 'showurl',
        'menu_url' => 'https://drive.google.com/open?id=11rTYbOZUAOphVS9svpy6EW-nrCs&usp=sharing',
        'aliases' => [qw(pho)],
    },
    'ellas' => {
        'type' => 'nomenu',
    },
    'lasperanza' => {
        'type' => 'nomenu',
    },
    'nepalindiathali' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/profile.php?id=100090161321553',
    },
    'namaskar' => {
        'type' => 'showurl',
        'menu_url' => 'http://www.namaskar.cz/denni-menu/',
    },
    'namaskarsmetanova' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/pg/Namaskarbrno/posts/',
        'aliases' => [qw(namaskar2)],
        'note' => "Pozor, obsahuje i menu Namaskaru Taborska. $bus32",
    },
    'jednabasen' => {
        'type' => 'menicka',
        'show_url' => 'https://www.menicka.cz/2657-jedna-basen-wine--coffee-bar.html',
        'aliases' => [qw(basen)],
    },
    'singha' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/pg/singhabrno/posts/',
    },
    'tao' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.taorestaurant.cz/tydenni_menu/nabidka/, https://www.facebook.com/pg/Táo-Viet-Nam-Home-Cooking-Sushi-Bar-1857934510936812/posts/',
    },
    'padagali' => {
        'type' => 'showurl',
        'menu_url' => 'http://padagali.cz/denni-menu/, https://www.menicka.cz/4116-padagali.html', # parseable
    },
    'tasteofindia' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.taste-of-india.cz/#daily-menu',
        'aliases' => [qw(taste)],
    },
    'naruzku' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.naruzkubrno.cz/tydenni-menu/',
    },
    'ukarla' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.ukarlabrno.cz/pages/poledni-menu, https://www.menicka.cz/6695-u-karla.html',
        'aliases' => [qw(karel)],
    },
    'pilgrim' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/pilgrims.cz/', # unparseable
    },
    'mahostina' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.mahostina.cz/dnesnibasta', # parseable
    },
    'umachala' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.menicka.cz/6559-bistro-u-machala.html, http://www.bistroumachala.cz/', # parseable
        'aliases' => [qw(machal sfinx)],
    },
    'triocasci' => {
        'type' => 'showurl',
        'menu_url' => 'https://triocasci.cz/jidlo/', # parseable
        'aliases' => [qw(ocasci ocas)],
    },
    'nachate' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.restauracenachate.cz/tydenni-menu.php', # parseable
    },
    'luzanka' => {
        'type' => 'showurl',
        'menu_url' => 'https://luzankabrno.cz/poledni-menu/', # parseable
    },
    'mayada' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/MAYADA.ghazal123/',
    },
    'casadelgusto' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/casadelgusto.brno/',
        'aliases' => [qw(casa gusto)],
    },
    'tesnevedle' => {
        'type' => 'showurl',
        'menu_url' => 'http://www.tesnevedle.cz/tydenni-menu', # parseable
    },
    'olymp' => {
        'type' => 'showurl',
        'menu_url' => 'http://www.olymp-pub.cz/edit/menu.pdf',
    },
    'ustaryhobilla' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.billexpress.cz/eshop/',
        'aliases' => [qw(bill ubilla)],
    },
    'fabrikfoodfactory' => {
        'type' => 'showurl',
        'menu_url' => 'https://factory.fabrik.cz/denni-menu/', # parseable
        'aliases' => [qw(fabrik)],
    },
    'efihostinec' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.efishop.cz/efi-hostinec/', # parseable
        'aliases' => [qw(efi)],
    },
    'prvnidoubravnicka' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.prvnidoubravnicka.cz/menu',
        'aliases' => [qw(doubravnicka)],
    },
    'nabotance' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/BistroNaBotance',
        'aliases' => [qw(cafefresh botanka)],
    },
    'podschody' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.facebook.com/profile.php?id=100094367065084',
        'aliases' => [qw(bistropodschody)],
    },
    'montenegro' => {
        'type' => 'showurl',
        'menu_url' => 'https://www.menicka.cz/4986-montenegro-pub.html',
        'aliases' => [qw(cernahora)],
    },
};

1;
