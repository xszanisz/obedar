#!/usr/bin/perl

use warnings;
use strict;
use utf8;

use Data::Dumper;
use LWP::UserAgent;
use LWP::Simple;
use JSON;
use POSIX qw(strftime);
use AnyEvent;
use AnyEvent::XMPP::Client;
use AnyEvent::XMPP::Util qw(node_jid);
use AnyEvent::XMPP::Ext::Disco;
use AnyEvent::XMPP::Ext::MUC;
use AnyEvent::XMPP::Ext::MUC::Message;
use Getopt::Long;
use File::Slurp qw(read_file write_file);

use Config::Cred;
use Config::Restaurants;

my $restaurants = $Config::Restaurants::restaurants;

my $jabber_info = $Config::Cred::jabber_info;

sub help {
    print STDERR <<EOF;
Invalid usage.
Usage: $0 [--join-muc|-m]

-m, --join-muc      Join MUC.
EOF
    return 1;
}

=head

Ak sa obedar nevie pripojit, asi skontrolovat, ze stale platia prihlasovacie udaje.

S parametrom --join-muc by mal funkcny vystup vyzerat takto:

Current commit is c0101d7ec1e19af0347bd8acec316531fb93c5e5
Commit from last run was c0101d7ec1e19af0347bd8acec316531fb93c5e5
Trying to connect...
session ready
MUC error: Bad value of cdata in tag <show/> qualified by namespace 'jabber:client'
ERROR: presence error: 400/bad-request (type modify): Bad value of cdata in tag <show/> qualified by namespace 'jabber:client'
Use of uninitialized value in subroutine entry at /usr/lib64/perl5/vendor_perl/Encode.pm line 204.
entered unix_obedy@chat.fi.muni.cz

=cut

my $join_muc = 0;
GetOptions('join-muc|m'        => \$join_muc,
) or help();

my $zomato_cache = {};

sub changelog {
    return unless $Config::Cred::gitlab_repo_url;

    # zistime aktualnu commitnutu verziu obedara
    my $current_latest_commit = qx[git rev-parse HEAD 2>&1];
    # ak nie sme v gitovom repozitari alebo sa nieco nepodari, nema cenu pokracovat
    return if ($? >> 8) != 0;
    print "Current commit is $current_latest_commit";

    my $rev_store = '.last-run-commit';

    # zistime, aku reviziu pouzival obedar pri poslednom spusteni
    my $last_run_rev;
    if (-r $rev_store && -w $rev_store) {
        $last_run_rev = read_file($rev_store);
        if (defined $last_run_rev and $last_run_rev !~  m/^[0-9a-f]{40}\n$/) {
            $last_run_rev = undef;
        }
    }
    if (defined $last_run_rev) {
        print "Commit from last run was $last_run_rev";
    }

    my $changelog_url;

    # ak su nove commity, nastavime URL
    if ($last_run_rev and $last_run_rev ne $current_latest_commit) {
        # toto je best effort, kedze su tu dva problemy:
        # - nezohladnuje to necommitovane lokalne zmeny
        # - lokalna historia gitu vyvojovej verzie nemusi byt totozna s gitlabovou historiou
        #   a vysledne URL moze odkazovat na commity, ktore este v gitlabe nie su...
        # snazim sa poctivo dodrzovat postup commit+push+restart, takze odkazem na posledny commit, nie HEAD
        $changelog_url = $Config::Cred::gitlab_repo_url . '/compare/' .
            substr($last_run_rev, 0, 8) . '...' . substr($current_latest_commit, 0, 8);
    }

    # ulozime aktualnu reviziu gitu pre buduci beh obedara
    write_file($rev_store, $current_latest_commit) or print STDERR "Failed to store current commit revision";
    chmod 0700, $rev_store;

    return $changelog_url;
}

sub get_menu_zomato {
    my ($info) = @_;

    my $ua = LWP::UserAgent->new();
    $ua->default_header('Accept' => 'application/json');
    $ua->default_header('user_key' => $Config::Cred::zomato_key);
    my $response = $ua->get('https://developers.zomato.com/api/v2.1/dailymenu?res_id=' . $info->{'site_id'});
    # 400 pouzivaju ako odpoved, ak dana restauracia neexistuje
    if (not $response->is_success and $response->code != 400) {
        return (1, 'Failed to fetch URL: ' . $response->status_line);
    }

    my $json = $response->decoded_content;
    my $menu_data = eval { from_json($json) };
    if ($@ or not $menu_data) {
        return (1, 'Failed to parse JSON: ' . $@);
    }

    if ($menu_data->{'status'} ne 'success') {
        # {"code":400,"status":"Bad Request","message":"No Daily Menu Available"}
        if ($menu_data->{'message'} eq 'No Daily Menu Available') {
            return (1, 'Kurwa! Cannot find daily menu.');
        } else {
            return (1, 'Failed to get menu: ' . $json);
        }
    }

    my $daily_menu = $menu_data->{'daily_menus'}->[0]->{'daily_menu'};
    if (not defined $daily_menu) {
        return (1, 'Kurwa! Restaurant has not provided menu for this day.');
    }

    if ($daily_menu->{'start_date'} ne strftime('%Y-%m-%d 00:00:00', localtime)) {
        return (1, 'Start date of the menu is not today');
    }

    my $dishes = $daily_menu->{'dishes'};
    if (not defined $dishes) {
        return (1, 'Failed to process returned JSON: Element dishes not found');
    }

    my $dishes_split = [];
    for my $dish (@$dishes) {
        $dish = $dish->{'dish'};

        # uz sa stalo (u Everestu a Himalaye), ze v jednej polozke su dve jedla naraz a oddelene \n,
        # tak to skusime takto osetrit. Normalne sa taky pripad nestava, takze by to nemalo ine veci rozbit.
        push @$dishes_split,
            map { {
                'name'  => $_,
                'price' => $dish->{'price'},
            } }
            split /\n/, $dish->{'name'};
    }

    if (not scalar @$dishes_split) {
        return (1, 'No dishes found');
    }

    my $menu = join "\n", map { ($_->{'name'} // '???') . ($_->{'price'} ? (' [' . $_->{'price'} . ']') : '') } @$dishes_split;
    $menu =~ s/[ \t]+/ /g;

    if ($menu eq '') {
        return (1, 'Zomato menu query yielded empty string');
    }

    return (0, $menu);
}

sub get_menu_zomato_cached {
    my ($info) = @_;

    my $site = $info->{'site_id'};
    my $today = strftime('%Y-%m-%d', localtime);

    if (not exists $zomato_cache->{$site}) {
        goto FETCH;
    }

    my $data = $zomato_cache->{$site};

    if ($data->{'day'} ne $today) {
        goto FETCH;
    }

    print "Zomato cache hit for $site\n";
    return (0, $data->{'result'});

    FETCH:

    print "Zomato cache miss for $site\n";
    my ($code, $result) = get_menu_zomato($info);
    if ($code == 0) {
        $zomato_cache->{$site} = {
            'day'       => $today,
            'result'    => $result,
        };
        return (0, $result);
    } else {
        my $fallback = defined $info->{fallback_url}
            ? "\n" . $info->{fallback_url}
            : '';
        return ($code, $result . $fallback);
    }
}

sub get_menu_showurl {
    my ($info) = @_;
    return $info->{'menu_url'};
}

sub normalize_restaurant {
    my ($rest) = @_;
    $rest =~ s/ //g;
    $rest =~ s/\?$//;
    $rest = lc $rest;

    for my $key (keys %$restaurants) {
        if (grep { $_ eq $rest } @{ $restaurants->{$key}->{'aliases'} }) {
            return $key;
        }
    }

    return $rest;
}

sub get_menu {
    my ($restaurant) = @_;

    $restaurant = normalize_restaurant($restaurant);

    my $prenote = '';
    my $note = '';

    my $rand_rest;
    if ($restaurant eq 'random' or $restaurant eq 'rnd') {
        my @rest_list = keys %$restaurants;
        $restaurant = $rest_list[rand @rest_list];
        $prenote = '--- ' . $restaurant . " ---\n";
    } elsif (not exists ($restaurants->{$restaurant})) {
        return 'Error: Co to kurwa jest za restauracju?';
    }

    my $info = $restaurants->{$restaurant};

    if (defined $info->{'note'}) {
        $note = "\n*** POZNAMKA: " . $info->{'note'};
    }

    if ($info->{'type'} eq 'zomato') {
        my ($stat, $msg) = get_menu_zomato_cached($info);
        print "{$restaurant} " . +($stat ? "NOK: $msg" : "OK") . "\n";
        if ($stat) {
            return $prenote . 'Zomato error: ' . $msg . $note;
        } else {
            return $prenote . $msg . $note;
        }

    } elsif ($info->{'type'} eq 'showurl' || $info->{'type'} eq 'menicka') {
        my $msg = get_menu_showurl($info);
        print "{$restaurant} OK\n";
        return $prenote . $msg . $note;

    } elsif ($info->{'type'} eq 'nomenu') {
        print "{$restaurant} OK\n";
        return $prenote . 'As usual. As everyday. Why you even ask?' . $note;

    } elsif ($info->{'type'} eq 'module') {
        eval "use $info->{'module'}";
        if ($@) {
            print STDERR "Error loading module: $@\n";
            return $prenote . 'Error loading module' . $note;
        }
        my $fn = $info->{'module'} . '::get_menu()';
        my ($stat, $msg) = eval "$fn";
        if ($@) {
            print STDERR "Error getting menu: $@\n";
            return $prenote . 'Error getting module menu' . $note;
        }

        my $empty_msg = (not defined $msg or $msg eq '');
        $msg //= '<undefined>';

        $msg =~ s/^\s+//;
        $msg =~ s/\s+$//;

        print "{$restaurant} " . +(($stat || $empty_msg) ? "NOK: $msg" : "OK") . "\n";

        if ($empty_msg) {
            $stat = 1;
            $msg = 'Menu query yielded empty string';
        }

        if ($stat) {
            my $fallback = eval ($info->{'module'} . '::my_url()');
            if ($@ or not $fallback) {
                print STDERR "Could not call expected my_url method: $@\n";
                $fallback = '"Error getting fallback URL"';
            }
            return $prenote . 'Error: ' . $msg . "\nTry " . $fallback . $note;
        } else {
            return $prenote . $msg . $note;
        }
    } else {
        print STDERR "Error: Unknown type in configuration '" . $info->{'type'} . "' for $restaurant\n";
        return "${prenote}Invalid configuration for restaurant ${restaurant}!${note}";
    }
}

sub obedar_reply {
    my ($chat_type, $text) = @_;

    my $prefix;
    if ($chat_type eq 'private') {
        $prefix = '';
    } elsif ($chat_type eq 'group') {
        $prefix = '@obed ';
    }

    my $private_prefix = 0;
    if ($chat_type eq 'private' and $text =~ m/^\@obed /) {
        $private_prefix = 1;
        $text =~ s/^\@obed //;
    }

    $text = lc $text;

    my $reply;
    if ($text eq "${prefix}help") {
        $reply = '@obed help - help
@obed list - list all restaurants
@obed all - show all menus (slow!)
@obed Everest - show menu for everest
@obed rnd/random - show menu for a random restaurant
@obed about - information about obedar'
    } elsif ($text eq "${prefix}about") {
        $reply = "https://gitlab.fi.muni.cz/xszanisz/obedar";
    } elsif ($text eq "${prefix}list") {
        $reply = join ', ', sort keys %$restaurants;
    } elsif ($text eq "${prefix}all") {
        # bolo by pekne, keby sme tu vedeli informovat dotazovatela, ze toto bude
        # pomale a ma vyckat, ale message callbacky AnyEvent::XMPP::Client a
        # AnyEvent::XMPP::Ext::MUC su "blokujuce" a spravy, ktore naposielame,
        # sa realne poslu az v momente, ked ukoncime callback proceduru
        print "--- all begin\n";
        $reply = '';
        for my $rest (sort keys %$restaurants) {
            my $menu = get_menu($rest);
            $reply .= "--- $rest ---\n$menu\n";
        }
        print "--- all end\n";
    } elsif ($text =~ m/$prefix(.*)/) {
        $reply = get_menu($1);
    }

    if ($private_prefix) {
        $reply = "Not necessary to use \@obed prefix in private conversions\n\n" . $reply;
    }

    # bot na svoje vlastne spravy nereaguje, takze netreba riesit, ci vo svojom vystupe
    # bude mat string @obed
    return $reply;
}

sub reply_to_msg {
    my ($msg_obj, $reply) = @_;

    my $repl = $msg_obj->make_reply;
    $repl->add_body($reply);
    $repl->send;
}

binmode STDOUT, ":encoding(utf8)";
binmode STDERR, ":encoding(utf8)";

my $changelog_url = $join_muc ? changelog() : undef;

my $backoff = 1;
my $backoff_limit = 1 << 10; # 17 minutes
our $client_exit = 0;

print STDERR "Trying to connect...\n";

while (1) {

    my $cl      = AnyEvent::XMPP::Client->new();
    my $disco   = AnyEvent::XMPP::Ext::Disco->new;
    my $muc     = AnyEvent::XMPP::Ext::MUC->new(disco => $disco);
    my $exit    = AnyEvent->condvar;
    $cl->add_extension($disco);
    $cl->add_extension($muc);
    # dokumentacia: https://metacpan.org/pod/AnyEvent::XMPP::Writer#METHODS
    $cl->set_presence('available', 'Na ukoncenie obedoveho marazmu', 64);
    $cl->add_account($jabber_info->{'login'}, $jabber_info->{'pass'}, undef, undef, { connect_timeout => 10 });
    $cl->reg_cb(
        'session_ready' => sub {
            my ($cl, $acc) = @_;
            print STDERR "session ready\n";

            $backoff = 1;

            return if not $join_muc;

            $muc->join_room($acc->connection, $jabber_info->{'room'}, $jabber_info->{'nick'} // node_jid($acc->jid));
            $muc->reg_cb(
                message => sub {
                    my ($cl, $room, $msg, $is_echo) = @_;
                    return if $is_echo or $msg->is_delayed;
                    my $text = $msg->any_body;

                    return if not defined $text or $text !~ m/\@obed /;

                    my $from = $msg->from;
                    $from =~ s{^unix_obedy\@chat\.fi\.muni\.cz/}{};
                    $from =~ s{@.*$}{};
                    print strftime('[grp] %Y-%m-%d %H:%M:%S ', localtime) . $from . ': ' . $msg->any_body . "\n";

                    my $reply = obedar_reply('group', $text);

                    reply_to_msg($msg, $reply);
                },
                error => sub {
                    my ($cl, $room, $error) = @_;
                    print STDERR "MUC error: " . $error->text . "\n";
                },
                join_error => sub {
                    my ($cl, $room, $error) = @_;
                    print STDERR "MUC join_error: " . $error->text . "\n";
                },
                enter => sub {
                    my ($cl, $room, $user) = @_;
                    print STDERR "entered " . $room->jid . "\n";

                    if ($changelog_url) {
                        $room->make_message('body' => "Psst, mam nejake novinky: $changelog_url")->send();
                    }

                },
            );

        },
        disconnect => sub {
            my ($cl, $acc, $h, $p, $reas) = @_;
            $h //= '<undefined>';
            $p //= '<undefined>';
            $reas //= '<undefined>';
            print STDERR "disconnect ($h:$p): $reas\n";
            $exit->send;
        },
        error => sub {
            my ($cl, $acc, $err) = @_;
            print STDERR "ERROR: " . $err->string . "\n";
        },
        message => sub {
            my ($cl, $acc, $msg) = @_;

            return if $msg->is_delayed;
            my $text = $msg->any_body;

            return if not defined $text;

            my $from = $msg->from;
            $from =~ s{^unix_obedy\@chat\.fi\.muni\.cz/}{};
            $from =~ s{@.*$}{};
            print strftime('[priv] %Y-%m-%d %H:%M:%S ', localtime) . $from . ': ' . $msg->any_body . "\n";

            my $reply = obedar_reply('private', $text);

            reply_to_msg($msg, $reply);
        },
    );


    $cl->start;

    my $iterm = AE::signal(TERM => sub { print STDERR "SIGTERM\n"; $cl->disconnect(); $client_exit = 1; $exit->send } );
    my $iint = AE::signal(INT => sub { print STDERR "SIGINT\n"; $cl->disconnect(); $client_exit = 1; $exit->send } );

    $exit->wait;

    # we only give up trying to reconnect when we got disconnected by user signal (SIGTERM/SIGINT)
    last if $client_exit;

    print STDERR "Sleeping $backoff seconds before trying to reconnect\n";
    sleep $backoff;
    $backoff <<= 1 if $backoff < $backoff_limit;

    print STDERR "Trying to reconnect...\n";
}

exit 0;
